const path = require('path');
const { Pool } = require('pg');
const pool = new Pool({
    user: 'ankur',
    host: 'localhost',
    database: 'postgres',
    password: 'ankur188',
    port: 5432
});
function dropTable() {
    return pool.query('DROP TABLE IF EXISTS beverages, companies');
}
function createTableCompanies() {
    return pool.query(
        'CREATE TABLE companies(company_id INT PRIMARY KEY, company_name VARCHAR, headquaters VARCHAR, net_worth_in_billions FLOAT)'
    );
}
function createTableBeverages() {
    return pool.query(
        'CREATE TABLE beverages(beverage_id INT, company_id INT, beverage_name VARCHAR, alchohol_percentage FLOAT, price FLOAT )'
    );
}
function insertCsvFileToCompaniesTable() {
    const companyPath = getPathFromCompaniesCsv();
    return pool.query(
        `COPY companies FROM '${companyPath}' with delimiter ',' csv header`
    );
}
function getPathFromCompaniesCsv() {
    const companyPath = path.resolve('./', 'sampleData/Company_Data.csv');
    return companyPath;
}
function insertCsvFileToBeveragesTable() {
    const beveragePath = getPathFromBeveragesCsv();
    return pool.query(
        `COPY beverages FROM '${beveragePath}' with delimiter ',' csv header`
    );
}
function getPathFromBeveragesCsv() {
    const beveragePath = path.resolve('./', 'sampleData/Beverages_Data.csv');
    return beveragePath;
}
// async function createTableAndInsertIntoTables() {
//     await createTableCompanies();
//     await insertCsvFileToCompaniesTable();
//     console.log('Successsfully created table companies');
//     await createTableBeverages();
//     await insertCsvFileToBeveragesTable();
//     console.log('Successsfully created table beverages');
// }
dropTable().then(() => createTableCompanies())
    .then(() => insertCsvFileToCompaniesTable())
    .then(() => {
        console.log('Successsfully created table companies')
        createTableBeverages()
    })
    .then(() => insertCsvFileToBeveragesTable()).
    then(() => console.log('Successsfully created table beverages'));