function errorHandler(err, req, res, next) {
  res.status(err.status).send(err.message);
  next(err.message);
}
module.exports = errorHandler;
