const company = require('../models/companies.js');
const joi = require('../utils/validation.js');

getCompanyData = function(req, res, next) {
  company
    .readCompanyData()
    .then(data => {
      if (data.rows.length == 0) {
        res.send({
          message: 'No companies are available in the database',
          status: 404
        });
        next('No companies are available in the database');
      } else {
        res.send(data.rows);
      }
    })
    .catch(err => res.send(err.message, err.status));
};

readCompanyDataById = function(req, res, next) {
  const result = joi.getData(req.params);
  if (result.error) {
    res.status(404).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    company
      .readCompanyDataById(req.params.id)
      .then(data => {
        if (data.rows.length == 0) {
          res.send({
            message: 'No company with this Id is available',
            status: 404
          });
          next('No company with this Id is available');
        } else {
          res.send(data.rows);
        }
      })
      .catch(err => res.send(err.message, err.status));
  }
};

readCompanyDataWithBeveragesById = function(req, res, next) {
  const result = joi.getData(req.params);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    company
      .readCompanyDataWithBeveragesById(req.params.id)
      .then(data => {
        if (data.rows.length == 0) {
          res.send({
            message: 'No company with this Id is available',
            status: 404
          });
          next('No company with this Id is available');
        } else {
          res.send(data.rows);
        }
      })
      .catch(err => res.send(err.message, err.status));
  }
};

insertData = function(req, res, next) {
  const result = joi.insertCompanyData(req.body);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    company
      .insertData(req.body)
      .then(data => res.send('successfully inserted'))
      .catch(err => res.send(err.message, err.status));
  }
};

deleteDataById = function(req, res, next) {
  const result = joi.getData(req.params);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    company
      .deleteDataById(req.params.id)
      .then(data => {
        if (data.rowCount == 0) {
          res.send({
            message: 'No company with this Id is available',
            status: 404
          });
          next('No company with this Id is available');
        } else {
          res.send('successfully deleted');
        }
      })
      .catch(err => res.send(err.message, err.status));
  }
};

updateDataById = function(req, res, next) {
  const result = joi.updateCompanyData(req.body);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    company
      .updateDataById(req.body)
      .then(data => {
        if (data.rowCount == 0) res.send('No company of this id is available');
        else res.send('updated');
      })
      .catch(err => res.send(err.message, err.status));
  }
};

module.exports = {
  getCompanyData,
  readCompanyDataById,
  readCompanyDataWithBeveragesById,
  insertData,
  deleteDataById,
  updateDataById
};
