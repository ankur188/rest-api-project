const beverages = require('../models/beverages.js');
const joi = require('../utils/validation.js');

readBeveragesData = function(req, res, next) {
  beverages
    .readBeveragesData()
    .then(data => {
      if (data.rows.length == 0) {
        res.send({
          message: 'No beverages are available in the database',
          status: 404
        });
        next('No beverages are available in the database');
      } else {
        res.send(data.rows);
      }
    })
    .catch(err => res.send(err.message, err.status));
};

readBeveragesDataById = function(req, res, next) {
  const result = joi.getData(req.params);
  if (result.error) {
    res.status(404).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    beverages
      .readBeveragesDataById(req.params.id)
      .then(data => {
        if (data.rows.length == 0) {
          res.send({
            message: 'No beverage with this Id is available',
            status: 404
          });
          next('No beverage with this Id is available');
        } else {
          res.send(data.rows);
        }
      })
      .catch(err => res.send(err.message, err.status));
  }
};

readBeveragesDataWithCompanyById = function(req, res, next) {
  const result = joi.getData(req.params);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    beverages
      .readBeveragesDataWithCompanyById(req.params.id)
      .then(data => {
        if (data.rows.length == 0) {
          res.send({
            message: 'No beverage with this Id is available',
            status: 404
          });
          next('No beverage with this Id is available');
        } else {
          res.send(data.rows);
        }
      })
      .catch(err => res.send(err.message, err.status));
  }
};

insertData = function(req, res, next) {
  const result = joi.insertBeverageData(req.body);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    beverages
      .insertData(req.body)
      .then(data => res.send('successfully inserted'))
      .catch(err => res.send(err.message, err.status));
  }
};

deleteDataById = function(req, res, next) {
  const result = joi.getData(req.params);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    beverages
      .deleteDataById(req.params.id)
      .then(data => {
        if (data.rowCount == 0) {
          res.send({
            message: 'No beverages with this Id is available',
            status: 404
          });
          next('No beverages with this Id is available');
        } else {
          res.send('successfully deleted');
        }
      })
      .catch(err => res.send(err.message, err.status));
  }
};

updateDataById = function(req, res, next) {
  const result = joi.updateBeverageData(req.body);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    next(result.error.details[0].message);
  } else {
    beverages
      .updateDataById(req.body)
      .then(data => {
        if (data.rowCount == 0) res.send('No beverage of this id is available');
        else res.send('updated');
      })
      .catch(err => res.send(err.message, err.status));
  }
};

module.exports = {
  readBeveragesData,
  readBeveragesDataById,
  readBeveragesDataWithCompanyById,
  insertData,
  deleteDataById,
  updateDataById
};
