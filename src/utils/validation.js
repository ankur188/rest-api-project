var joi = require('joi');

getData = function(id) {
  var getSchema = {
    id: joi
      .number()
      .integer()
      .min(1)
  };
  return joi.validate(id, getSchema);
};

insertCompanyData = function(body) {
  var getCompanySchema = {
    company_id: joi
      .number()
      .integer()
      .min(1),
    company_name: joi
      .string()
      .min(3)
      .max(100)
      .required(),
    headquaters: joi
      .string()
      .min(3)
      .max(100)
      .required(),
    net_worth_in_billions: joi
      .number()
      .min(1)
      .required()
  };
  return joi.validate(body, getCompanySchema);
};

updateCompanyData = function(body) {
  var getCompanySchema = {
    company_id: joi
      .number()
      .integer()
      .min(1)
      .required(),
    company_name: joi
      .string()
      .min(3)
      .max(100)
      .required(),
    headquaters: joi
      .string()
      .min(3)
      .max(100)
      .required(),
    net_worth_in_billions: joi
      .number()
      .min(1)
      .required()
  };
  return joi.validate(body, getCompanySchema);
};

insertBeverageData = function(body) {
  var getBeverageSchema = {
    beverage_id: joi
      .number()
      .integer()
      .min(1),
    company_id: joi
      .number()
      .min(1)
      .required(),
    beverage_name: joi
      .string()
      .min(3)
      .max(100)
      .required(),
    alchohol_percentage: joi
      .number()
      .min(1)
      .required(),
    price: joi
      .number()
      .min(1)
      .required()
  };
  return joi.validate(body, getBeverageSchema);
};

updateBeverageData = function(body) {
  var getBeverageSchema = {
    beverage_id: joi
      .number()
      .integer()
      .min(1)
      .required(),
    company_id: joi
      .number()
      .min(1)
      .required(),
    beverage_name: joi
      .string()
      .min(3)
      .max(100)
      .required(),
    alchohol_percentage: joi
      .number()
      .min(1)
      .required(),
    price: joi
      .number()
      .min(1)
      .required()
  };
  return joi.validate(body, getBeverageSchema);
};

module.exports = {
  getData,
  insertCompanyData,
  updateCompanyData,
  insertBeverageData,
  updateBeverageData
};
