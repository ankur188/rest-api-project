const express = require('express');
const Router = express.Router();
Router.use(express.json());

const fs = require('fs');

const companiesData = require('../controller/companies.js');

Router.get('/', companiesData.getCompanyData);

Router.get('/:id', companiesData.readCompanyDataById);

Router.get('/:id/beverages', companiesData.readCompanyDataWithBeveragesById);

Router.post('/', companiesData.insertData);

Router.delete('/:id', companiesData.deleteDataById);

Router.put('/', companiesData.updateDataById);

module.exports = Router;
