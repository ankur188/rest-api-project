const express = require('express');
const Router = express.Router();
Router.use(express.json());

const beveragesData = require('../controller/beverages.js');

Router.get('/', beveragesData.readBeveragesData);

Router.get('/:id', beveragesData.readBeveragesDataById);

Router.get('/:id/company', beveragesData.readBeveragesDataWithCompanyById);

Router.post('/', beveragesData.insertData);

Router.delete('/:id', beveragesData.deleteDataById);

Router.put('/', beveragesData.updateDataById);

module.exports = Router;
