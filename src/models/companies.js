const { Pool } = require('pg');
const pool = new Pool({
  user: 'ankur',
  host: 'localhost',
  database: 'postgres',
  password: 'ankur188',
  port: 5432
});

function readCompanyData() {
  try {
    return pool
      .query('SELECT * FROM companies order by company_id')
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function readCompanyDataById(id) {
  try {
    return pool
      .query(
        'SELECT * FROM companies where company_id = $1 order by company_id',
        [id]
      )
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function readCompanyDataWithBeveragesById(id) {
  try {
    return pool
      .query(
        'select * from companies FULL JOIN beverages ON companies.company_id = beverages.company_id where companies.company_id = $1 order by companies.company_id',
        [id]
      )
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function insertData(input) {
  try {
    return pool
      .query(
        `INSERT INTO companies (company_id, company_name, headquaters, net_worth_in_billions)
    VALUES ((select max(company_id) from companies)+1, $1, $2, $3 )`,
        [
          input['company_name'],
          input['headquaters'],
          input['net_worth_in_billions']
        ]
      )
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function deleteDataById(id) {
  try {
    return pool
      .query('DELETE FROM companies WHERE company_id = $1', [id])
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function updateDataById(input) {
  try {
    const formatInput = Object.entries(input).reduce((acc, curr) => {
      if (curr[1] != '') acc[curr[0]] = curr[1];
      else acc[curr[0]] = null;
      return acc;
    }, {});
    return pool
      .query('select * from companies where company_id = $1', [
        formatInput['company_id']
      ])
      .then(detailsOfCompany => {
        if (detailsOfCompany.rowCount == 0) {
          throw detailsOfCompany;
        } else {
          pool.query(
            'update companies set company_name = $2, headquaters = $3, net_worth_in_billions = $4 where company_id = $1',
            [
              formatInput['company_id'],
              formatInput['company_name'] ||
                detailsOfCompany.rows[0]['company_name'],
              formatInput['headquaters'] ||
                detailsOfCompany.rows[0]['headquaters'],
              formatInput['net_worth_in_billions'] ||
                detailsOfCompany.rows[0]['net_worth_in_billions']
            ]
          );
        }
      })
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

module.exports = {
  readCompanyData,
  readCompanyDataById,
  readCompanyDataWithBeveragesById,
  insertData,
  deleteDataById,
  updateDataById
};
