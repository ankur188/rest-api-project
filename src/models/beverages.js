const { Pool } = require('pg');
const pool = new Pool({
  user: 'ankur',
  host: 'localhost',
  database: 'postgres',
  password: 'ankur188',
  port: 5432
});

function readBeveragesData() {
  try {
    return pool
      .query('SELECT * FROM beverages order by beverage_id')
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function readBeveragesDataById(id) {
  try {
    return pool
      .query(
        'SELECT * FROM beverages where beverage_id = $1 order by beverage_id',
        [id]
      )
      .catch(error => error);
  } catch (error) {
    return errror;
  }
}

function readBeveragesDataWithCompanyById(id) {
  try {
    return pool
      .query(
        'select * from beverages FULL JOIN companies ON beverages.company_id = companies.company_id where beverages.beverage_id = $1 order by beverages.beverage_id',
        [id]
      )
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function insertData(input) {
  try {
    return pool
      .query(
        `INSERT INTO beverages (beverage_id, company_id, beverage_name, alchohol_percentage, price)
    VALUES ((select max(beverage_id) from beverages)+1, $1, $2, $3, $4 )`,
        [
          input['company_id'],
          input['beverage_name'],
          input['alchohol_percentage'],
          input['price']
        ]
      )
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function deleteDataById(id) {
  try {
    return pool
      .query('DELETE FROM beverages WHERE beverage_id = $1', [id])
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

function updateDataById(input) {
  try {
    const formatInput = Object.entries(input).reduce((acc, curr) => {
      if (curr[1] != '') acc[curr[0]] = curr[1];
      else acc[curr[0]] = null;
      return acc;
    }, {});

    return pool
      .query('select * from beverages where beverage_id = $1', [
        formatInput['beverage_id']
      ])
      .then(detailsOfBeverage => {
        if (detailsOfBeverage.rowCount == 0) {
          throw detailsOfBeverage;
        } else {
          pool.query(
            'update beverages set company_id = $2, beverage_name = $3, alchohol_percentage = $4, price = $5 where beverage_id = $1',
            [
              formatInput['beverage_id'],
              formatInput['company_id'] ||
                detailsOfBeverage.rows[0]['company_id'],
              formatInput['beverage_name'] ||
                detailsOfBeverage.rows[0]['beverage_name'],
              formatInput['alchohol_percentage'] ||
                detailsOfBeverage.rows[0]['alchohol_percentage'],
              formatInput['price'] || detailsOfBeverage.rows[0]['price']
            ]
          );
        }
      })
      .catch(error => error);
  } catch (error) {
    return error;
  }
}

module.exports = {
  readBeveragesData,
  readBeveragesDataById,
  readBeveragesDataWithCompanyById,
  insertData,
  deleteDataById,
  updateDataById
};
