const companies = require('./routes/companies.js');
const beverages = require('./routes/beverages.js');
const express = require('express');
const logger = require('./middleware/logging.js');
const errorHandler = require('./middleware/errorHandler.js');
const app = express();
app.use(express.json());

app.use(logger.log);
app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' });
});
app.use('/api/companies', companies, errorHandler);
app.use('/api/beverages', beverages, errorHandler);
app.use('*', (req, res) => res.status(404).send('invalid url- 404'));
app.use(logger.error);
// app.use((err, req, res, next) => {
//   res.send(`Error: ${err.message} status: ${err.statusCode}`)
// })
const port = process.env.Port || 8060;
app.listen(port, () => console.log(`Listening on port ${port}`));
